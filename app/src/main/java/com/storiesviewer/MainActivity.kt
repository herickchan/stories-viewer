package com.storiesviewer

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_scrolling.*
import okhttp3.*
import java.io.IOException
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.SearchView
import android.view.Menu
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    // data that stores json data as StoryFeed class
    lateinit var data: StoryFeed
    // list used to display stories on the UI
    var displayList: MutableList<Story> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)
        setSupportActionBar(toolbar)

        // Setting up recycler view
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        story_feed.layoutManager = layoutManager

        // Setting dividers for recycler view
        val dividerItemDecoration = DividerItemDecoration(story_feed.context, layoutManager.orientation)
        story_feed.addItemDecoration(dividerItemDecoration)

        // Fetch data from API
        fetchStories()
    }

    // For search
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem = menu?.findItem(R.id.action_search)

        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView

            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(string: String?): Boolean {
                    // Clear display list
                    displayList.clear()

                    // Only show items that match search text
                    // If search string is empty, show everything
                    if (string!!.isNotEmpty()) {
                        val searchText = string.toLowerCase()
                        data.stories.forEach {
                            if (it.title.toLowerCase().contains(searchText)) {
                                displayList.add(it)
                            }
                        }
                    } else {
                        displayList.addAll(data.stories)
                    }

                    story_feed.adapter?.notifyDataSetChanged()
                    return true
                }
            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    // Using OkHttp to fetch stories from url via Http request
    private fun fetchStories() {
        val url = "https://www.wattpad.com/api/v3/stories?offset=0&limit=30&fields=stories(id,title,cover,user)"
        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                data = GsonBuilder().create().fromJson(body, StoryFeed::class.java)
                displayList.addAll(data.stories)

                runOnUiThread {
                    story_feed.adapter = RecyclerAdapter(StoryFeed(displayList))
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                Toast.makeText(this@MainActivity, "Failed to fetch stories", Toast.LENGTH_SHORT).show()
            }
        })
    }
}

data class StoryFeed(val stories: List<Story>)

data class Story(val id: Int, val title: String, val user: User, val cover: String)

data class User(val name: String, val avatar: String, val fullname: String)
