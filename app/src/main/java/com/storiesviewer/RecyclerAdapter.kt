package com.storiesviewer

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.stories_row.view.*

class RecyclerAdapter(val storyFeed: StoryFeed): RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return storyFeed.stories.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val card = LayoutInflater.from(parent.context).inflate(R.layout.stories_row, parent, false)
        return ViewHolder(card)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val storyTitle = storyFeed.stories[position]

        // Load story contents
        holder.view.story_title.text = storyTitle.title
        Picasso.get().load(storyTitle.cover).into(holder.view.story_image)

        // Load author user information
        holder.view.user_name.text = storyTitle.user.name
        Picasso.get().load(storyTitle.user.avatar).into(holder.view.user_avatar)
    }
}

class ViewHolder(val view: View): RecyclerView.ViewHolder(view)